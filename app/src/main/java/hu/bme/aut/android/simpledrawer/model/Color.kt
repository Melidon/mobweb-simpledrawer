package hu.bme.aut.android.simpledrawer.model

enum class Color(val rgb: Int) {
    RED(android.graphics.Color.RED),
    GREEN(android.graphics.Color.GREEN),
    BLUE(android.graphics.Color.BLUE)
}
